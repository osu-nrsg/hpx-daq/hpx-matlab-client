# MATLAB viewer client for hpx-radar-server

Sample MATLAB client for hpx-radar-server. Receives ZeroMQ messages from the server and animates a plan-view radar plot.

## Requirements

* matlab-zmq (GitHub, randallpittman version)
  * Requires the ZeroMQ C development library to build.
* msgpack-matlab2 (GitHub)
  * Requires msgpack-c 1.1 or greater.

See [README-DEPENDENCIES.md](README-DEPENDENCIES.md) for dependency setup instructions.

## Setup for running the hpx-radar-server

### Download hpx-matlab-client

```bash
cd $SOURCES
git clone https://gitlab.com/osu-nrsg/hpx-matlab-client.git
cd hpx-matlab-client
```

### Copy the zmq and msgpack mex libraries into the `hpx-matlab-client` directory

```bash
cp  $SOURCES/msgpack-matlab2/msgpack.cc ./
cp -r $SOURCES/matlab-zmq/lib/+zmq ./
```

### Edit `mat_config.toml` as desired

This is the configuration for the radar server.

### Start the HPx Radar Server

We'll use an interprocess communication socket AKA Unix Domain Socket to talk
between the server and client. (N.b. on Windows you have to use a tcp socket
instead). This looks like a file in the Linux filesystem, so you have to have
a place for it to exist in your filesystem for the duration that the server
is running. In the below command, specify the path to the socket file you want
the server to create (the directory must exist) after `ipc://`. To specify an
absolute path, add a third `/`, e.g. `ipc:///home/pittmara/sockets/WIMR.sock`.
Or to specify a path relative to the current directory, just use the two
slashes, e.g. `ipc://WIMR.sock` or `ipc://sockets/WIMR.sock`. The socket name
and extension are irrelevant, so long as the same socket is used in the client.

Alternatively, specify a TCP socket >1024, e.g. `tcp://127.0.0.1:10000`,
where `127.0.0.1` is the loopback IP address to your own PC and `10000` is the
TCP port to use. It is possible to run the server and client on separate PCs,
specifying the server's IP address in the endpoint.

```bash
cd ~/src/hpx-radar-server  # or as appropriate
HPXCONFIGPATH="~/SOURCES/hpx-matlab-client/mat_config.toml" \
HPXPRINTLEVEL=6 \
HPXENDPOINT="ipc:///shared/sockets/WIMR.sock" \
bin/hpx-radar-server
```

### In MATLAB, run the `hpx-matlab-client` script.

Note that you'll need to edit the `endpoint` variable in the script to match
`HPXENDPOINT` from the previous step.

You should see a figure open and some number of rotations plotted.
